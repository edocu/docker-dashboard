const express = require('express')
const path = require('path')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const docker = require('./dockerapi')
const compose = require('./dockercompose')
const exec = require('child_process').exec

const port = process.env.PORT || 3000

app.use(express.static('public'))

app.get('/images', (req, res) => {
    res.status(200).send(compose.listImages())
})

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'))
})

server.listen(port, () => {
    console.log(`Server started on port ${port}`)
})

io.on('connection', socket => {
    socket.on('containers.list', () => {
        refreshContainers()
    })

    socket.on('container.start', args => {
        const container = docker.getContainer(args.id)

        if (container) {
            container.start((err, data) => refreshContainers())
        }
    })

    socket.on('container.stop', args => {
        const container = docker.getContainer(args.id)

        if (container) {
            container.stop((err, data) => refreshContainers())
        }
    })

    socket.on('container.create', args => {
        const image = compose.getImage(args.image)
        exec(`docker-compose up -d ${args.image}`, (err, stderr, stdout) => {
            if (err) {
                return console.error(`exec error: ${err}`)
            }
            console.log(`stdout: ${stdout}`)
            console.log(`stderr: ${stderr}`)
        })

    })
})

setInterval(refreshContainers, 2000)

function refreshContainers() {
    docker.listContainers({all: true}, (err, containers) => {
        io.emit('containers.list', containers)
    })
}

/* TODO
 * possibility to create container collections and run them
 * store those collections somehow - some very simple storage like redis
*/
