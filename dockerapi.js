const Docker = require('dockerode')

let options = {
    socketPath: '/var/run/docker.sock'
}

module.exports = new Docker(options)

