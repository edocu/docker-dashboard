import * as React from 'react'
import * as _ from 'lodash'
import * as io from 'socket.io-client'
import {Container, ContainerListItem} from './containerListItem'
import {ContainerList} from './containerList'
import {Image, ImageListItem} from './imageListItem'
import {ImageList} from './imageList'

let socket = io.connect()

class AppState {
    containers?: Container[]
    stoppedContainers?: Container[]
    images?: Image[]
}

export class AppComponent extends React.Component<{}, AppState> {
    constructor() {
        super()
        this.state = {
            containers: [],
            stoppedContainers: [],
            images: []
        }

        socket.on('containers.list', (containers: any) => {
            const partitioned = _.partition(containers, (c:any) => {
                return c.State === 'running'
            })

            this.setState({
                containers: partitioned[0].map(this.mapContainer),
                stoppedContainers: partitioned[1].map(this.mapContainer)
            })
        })
    }

    mapContainer(container:any): Container {
        return {
            id: container.Id,
            name: _.chain(container.Names)
                .map((n: string) => n.substr(1))
                .join(", ")
                .value(),
            state: container.State,
            status: `${container.State} (${container.Status})`,
            image: container.Image
        }
    }

    mapImage(image:any): Image {
        return {...image}
    }

    componentDidMount() {
        socket.emit('containers.list')

        fetch('http://localhost:3000/images')
        .then(result => {
            return result.json()
        })
        .then(body => {
            this.setState({
                images: _.map(body, this.mapImage)
            })
        })
    }

    render() {
        return (
            <div className="container">
                <h1 className="page-header">Docker Dashboard</h1>

                <ImageList images={this.state.images} />

                <ContainerList title="Running" containers={this.state.containers} />
                <ContainerList title="Stopped" containers={this.state.stoppedContainers} />
            </div>
        )
    }
}
