import * as React from 'react'
import {Image, ImageListItem} from './imageListItem'

export class ImageListProps {
    images: Image[]
}

export class ImageList extends React.Component<ImageListProps, {}> {
    render() {
        return (
            <ul className="list-unstyled">
                {this.props.images.map(i => <ImageListItem key={i.image} {...i}/>)}
            </ul>
        )
    }
}