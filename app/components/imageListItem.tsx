import * as React from 'react'
import * as io from 'socket.io-client'

const socket = io.connect()

export interface Image {
    image: string
    command?: string
    containerName: string
    networks?: any
    ports: any
    volmes: any
}

export class ImageListItem extends React.Component<Image, {}> {
    createContainer() {
        socket.emit('container.create', {image: this.props.image})
    }

    render() {
        return (
            <li>
                <span>{this.props.image} - {this.props.containerName}</span>
                <button
                    className="btn btn-default"
                    onClick={this.createContainer.bind(this)}>
                    Run container
                </button>
            </li>
        )
    }
}