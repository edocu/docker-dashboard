const yaml = require('js-yaml')
const fs   = require('fs')
const _ = require('lodash')

const compose = function() {}

var composeFile = yaml.safeLoad(fs.readFileSync('/home/krym/docker-compose.yml', 'utf8'));

compose.getImage = function(id) {
    return composeFile.services[id]
}

compose.listImages = function() {
    return _.map(composeFile.services, (service, key) => {
        return {
            image: key,
            containerName: service.container_name,
            volumes: service.volumes,
            command: service.command,
            networks: service.networks,
            ports: service.ports
        }
    })
}

module.exports = compose